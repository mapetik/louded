# louded
External development packages for LCSEE-optimized Ubuntu Distribution.

## To-do

* lcsee-git-devscripts needs to be uploaded to LCSEE apt repository, then added to louded-software.
 - Side note: look into lcsee-deploy, see if it can be fixed. If not, remove it and document package deployment process.
* Figure out how to perform a preseeded install on a wireless network.
